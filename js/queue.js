function GenerateTiket(attractionID) {
    /*
    The format of the string to enter the queue is in the following format:
    ID#XXXXXYYYYMMDDHHMMSS
    ID: The attraction ID
    #: delimiter to split the ID from the rest of the string
    XXXXX: the ticket number
    YYYY: year
    MM: month
    DD: day
    HH: hour
    MM: minute
    SS: second
    */
    var value = String(attractionID) + "#";
    value += getRandomBase32(5);
    value += getDate();
    return value;
}

function getTicketElements(ticketString) {
    var value = {};
    value['ID'] = ticketString.substr(0,ticketString.indexOf('#'));
    ticketString = ticketString.substr(ticketString.indexOf('#')+1);
    value['ticket'] = ticketString.substr(0,5);
    value['ticket'] = value['ticket'].substr(0,2) + "-" + value['ticket'].substr(2,5);
    value['dateString'] = ticketString.substr(5);

    return value;
}

function getDate() {
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
        hour = d.getHours();
        minute = d.getMinutes();
        second = d.getSeconds();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    if (hour.length < 2) 
        hour = '0' + hour;
    if (minute.length < 2) 
        minute = '0' + minute;
    if (second.length < 2) 
        second = '0' + second;

    return [year, month, day, hour, minute, second].join('');
}

function getRandomBase32(length) {
    let b32 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var value = "";
    for (var i=0; i<length; i++) {
        var r = Math.round(Math.random() * 32);
        value += b32[r];
    }
    return value;
}

function encryptTicket(ticket) {
    let publicKey = `-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKCsWE6e/3ro4kz589sMIyo48kJAs2ZR
6ifDxjF2b25XR5sJjlyXQpv4xgLOqSr6b5CefEEsXRTlbR9TbltdDJkCAwEAAQ==
-----END PUBLIC KEY-----`;

    // Create the encryption object.
    var crypt = new JSEncrypt();

    crypt.setPublicKey(publicKey);
    return crypt.encrypt(ticket);
}

function decryptTicket(ticket) {
    let privateKey = `-----BEGIN RSA PRIVATE KEY-----
MIIBPQIBAAJBAKCsWE6e/3ro4kz589sMIyo48kJAs2ZR6ifDxjF2b25XR5sJjlyX
Qpv4xgLOqSr6b5CefEEsXRTlbR9TbltdDJkCAwEAAQJBAIDFwDy6581tSrog9hVQ
I/raCyeYTXArJLzcBkNsRaCrY9s1ATMn3S2DW39cyK0QSKGmi2ZypcCz2mr2LFJ7
U5UCIQDO3QE9lBsDJEYfJVfzWkxwLsb89xeymmZ/WviZg4K7kwIhAMbWmjK/5HRa
Ip6B/VOIyRGMJCh3X5TJvzHt0Z2xMKqjAiEAhCz8hO8kT16XG4GN7+VLoAaTiMEN
OEalpUxoOhaoqtcCIQC9saC8R9uwsQkUBVljpzyD1BnuzSvPq8UO4h63fYK7VwIh
AKgG9feeDFVz+rJvizxJU3FAQDoHLMS549pRJq81Sj3w
-----END RSA PRIVATE KEY-----`;

    // Create the encryption object.
    var crypt = new JSEncrypt();

    crypt.setPrivateKey(privateKey);
    return crypt.decrypt(ticket);
}

function MakeQRCode(divID, ticketStringEnc, ticketID, ticketStringRaw) {
    var qrcode = new QRCode(divID);
    qrcode.makeCode(ticketStringEnc);
    let t = getTicketElements(ticketStringRaw)["ticket"];
    document.getElementById(ticketID).innerText = t;
}

function makeScanHandler(attractionID, queueID) {
    let handler = function onScanSuccess(decodedText, decodedResult) {
        // handle the scanned code
        var ticketRaw = decryptTicket(decodedText);
        var ticket = getTicketElements(ticketRaw);

        // Check if the ticket if for the right attraction
        if (attractionID != ticket["ID"]) {
            console.log("Invalid Attraction ID", attractionID, ticket["ID"]);
            return;
        }
        
        // Check if the ticket is already added to the list
        if (document.getElementById("ticket-"+ticket["ticket"]) != undefined) {
            return;
        }

        console.log(ticketRaw, ticket);

        // Add the ticket to the list
        document.getElementById(queueID).innerHTML +=
            `<li id="ticket-`+ticket["ticket"]+`">`+
            "<pre style='font-size:18px;'>"+ticket["ticket"]+"</pre>" +
            ticket["dateString"]+
            ` <button onclick="document.getElementById('ticket-`+ticket["ticket"]+`').remove()">Bump</button>`+
            "</li>";
    }
    return handler;
}